<?php
// https://www.tutorialspoint.com/php/php_and_xml.htm
// 25/11/2019
$canal = array('titulo' => "¿Qué hay para cenar?",
    'enlace' => 'http://menu.ejemplo.com/',
    'descripcion' => 'Elegir la comida de hoy');

print "<canal>\n";

foreach ($canal as $elemento => $contenido) {
  print " <$elemento>";
  print htmlentities($contenido);
  print "</$elemento>\n";
}

print "</channel>";
?>
