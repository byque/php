<?php declare(strict_types=1);  // Uso de tipos en métodos (string $cadena)
final class Correo  // Clase de la que no se puede heredar
{
    private $correo;

    private function __construct(string $correo)  // Patrón singleton
    {
        $this->asegurarCorreoValido($correo);

        $this->correo = $correo;
    }

    public static function desdeCadena(string $correo): self
    {
        return new self($correo);
    }

    public function __toString(): string  // Método mágico
    {
        return $this->correo;
    }

    private function asegurarCorreoValido(string $correo): void
    {
        if (!filter_var($correo, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidArgumentException(
                sprintf(
                    '"%s" no es una dirección de correo válida',
                    $correo
                )
            );
        }
    }
}
