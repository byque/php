<?php
	// Example 4-28. A while loop
	
	$fuel = 10;
	
	while ($fuel > 1)
	{
		// Keep driving ...
		echo "There's enough fuel<br>";
		$fuel--;
	}
	
?>
