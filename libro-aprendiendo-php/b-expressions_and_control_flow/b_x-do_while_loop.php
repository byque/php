<?php
	// Example 4-32. Expanding Example 4-31 to use curly braces
	
	$count = 1;
	
	do
	{
		echo "$count times 12 is " . $count * 12;
		echo "<br>";
	} while (++$count <= 12);
	
?>
