<?php
	// Example 4-33. Outputting the times table for 12 from a for loop
	// The only difference between Ex 4-33 and 4-34 are the curly braces.
	
	for ($count = 1; $count <= 12 ; $count++)
	{
		echo "$count times 12 is " . $count * 12 . "<br>";
	}
	
?>
