<?php
	// Example 4-22. A multiple-line if ... elseif ... statement
	// curly braces
	
	if		($page == "Home")	echo "You selected Home";
	elseif	($page == "About")	echo "You selected About";
	elseif	($page == "News")	echo "You selected News";
	elseif	($page == "Login")	echo "You selected Login";
	elseif	($page == "Links")	echo "You selected Links";
	
?>
