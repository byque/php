<?php
	// Example 4-29. A while loop to print the 2 times table
	
	$count = 1;
	
	while ($count <= 12)
	{
		echo "$count times 12 is " . $count * 12 . "<br>";
		$count++;
	}
	
	
?>
