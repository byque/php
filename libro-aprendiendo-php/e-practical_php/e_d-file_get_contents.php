<?php
	/* Example 7-13. Using file_get_contents */
	echo "<pre>";	// Enables display of line feeds
	echo file_get_contents("testfile.txt");
	echo "</pre>";	// Terminates pre tag
?>