<?php
	/* Example 5-27. Creating a final method */
	class User
	{
		final function copyright()
		{
			echo "This class was written by Joe Smith";
		}
	}
?>