<?php
	/* Example 5-16. Creating a destructor method in PHP5 */	
 	class User
	{
		function __destruct($param1, $param2)
		{
			// Destructor code goes here
		}
	}
?>