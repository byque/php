<?php
	// Example 11-8. Using select with the multiple attribute
	
	echo '
	Vegetables
	<select name="veg" size="5" multiple="multiple">
		<option value="Peas">Peas</option>
		// Default selection
		<option selected="selected" value="Beans">Beans</option>
		<option value="Carrots">Carrots</option>
		<option value="Cabbage">Cabbage</option>
		<option value="Broccoli">Broccoli</option>
	</select>
	'
	
?>
