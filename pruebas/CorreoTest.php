<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;

final class CorreoTest extends TestCase
{
    public function testPuedeCrearseDesdeDireccionCorreoValida(): void
    {
        $this->assertInstanceOf(
            Correo::class,
            Correo::desdeCadena('usuario@ejemplo.com')
        );
    }

    public function testNoPuedeCrearseDesdeDireccionCorreoInvalida(): void
    {
        $this->expectException(InvalidArgumentException::class);

        Correo::desdeCadena('invalid');
    }

    public function testPuedeUsarseComoCadena(): void
    {
        $this->assertEquals(
            'usuario@ejemplo.com',
            Correo::desdeCadena('usuario@ejemplo.com')
        );
    }
}
